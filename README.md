LocalPhotoMultipleSelect
========================

本地图片多图选择器。

### 主要类说明

-   [ImageWorker][1]：模仿`BitmapFun`内`ImageWorker`写的一个图片异步加载类，解决`java.lang.OutofMemoryError: bitmap size exceeds VM budget.`

[1]: <http://git.oschina.net/oscharles/localphotomultipleselect/blob/develop/ImageBrowse/src/android/apps/ImageWorker.java>

-   [SquareLayout][2]:`GridView`图片等宽的插件。

[2]: <http://git.oschina.net/oscharles/localphotomultipleselect/blob/develop/ImageBrowse/src/android/apps/SquareLayout.java>

### 出现问题解决

-   [ImageWorker][1]`#loadImage(long,ImageView)`内，修改`executeOnExecutor(Executor,Params...)`方法内的`Executor`参数可解决图片加载过慢的问题，但同时会加重内存的付出，需要根据实际情况修改。

### 一些参数的说明

-   [GridImage][3]`#clazz`：通过上次视图跳转并传递过来，作用为选择图片确定后需要跳转的视图。通过`Bundle`传递，`Key`为[GridImage][3]`#INTENT_CLAZZ`。

[3]: <http://git.oschina.net/oscharles/localphotomultipleselect/blob/master/ImageBrowse/src/android/apps/GridImage.java>
